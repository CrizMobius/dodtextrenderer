#ifdef _DEBUG
	#define new new( _CLIENT_BLOCK, __FILE__, __LINE__)
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <font.hpp>
#include <text_renderer.hpp>

#include <Windows.h>
#include <cassert>

#define GLEW_STATIC
#include <glew.h>

using namespace DODTextRenderer;

HGLRC g_hrc = nullptr;
HWND g_hwnd = nullptr;

void testCreateAndDestroyFont()
{
	Font font = {};
	assert(font::createFont(font, "Lucida Grande Regular.sdff.png","Lucida Grande Regular.sdff.txt"));
	font::destroyFont(font);
}
void testConvertTextToMetrics()
{
	Font font;
	font::createFont(font, "Lucida Grande Regular.sdff.png","Lucida Grande Regular.sdff.txt");

	const wchar_t * wText = L"This is a test string ���";
	std::size_t textLength = std::wcslen(wText);
	std::vector<Metrics> metricsBuffer;
	bool result = textRenderer::convertTextToMetrics(wText, textLength, font, metricsBuffer);
	assert(result);

	// Check that generated ids are correct
	for(uint32_t i = 0; i < textLength; ++i)
	{
		assert(wText[i] == metricsBuffer[i]._id);
	}
	font::destroyFont(font);
}
void testConvertTextToPrimitives()
{
	Font font;
	font::createFont(font, "Lucida Grande Regular.sdff.png","Lucida Grande Regular.sdff.txt");

	const wchar_t * wText = L"This is a test string";
	std::size_t textLength = std::wcslen(wText);
	std::vector<Metrics> metricsBuffer;
	metricsBuffer.reserve(textLength);
	std::vector<Vec3> verticies;
	std::vector<Vec2> texCoords;
	std::vector<uint32_t> indencies;
	textRenderer::convertTextToMetrics(wText, textLength, font, metricsBuffer);
	bool result = textRenderer::convertTextToPrimitives(wText, textLength, font, 10.0f, metricsBuffer, verticies, texCoords, indencies);
	assert(result);
	assert(verticies.size() == texCoords.size());
	font::destroyFont(font);
}
void testConstructTextRenderComponent()
{
	Font font;
	font::createFont(font, "Lucida Grande Regular.sdff.png","Lucida Grande Regular.sdff.txt");

	wchar_t * wText = L"This is a test string";
	std::size_t textLength = std::wcslen(wText);
	TextRenderer textRenderer = {};
	Transform4 transform = {};
	ID id = textRenderer::constructTextRenderComponent(textRenderer, font, transform, wText, textLength);
	assert(id != 0);
	
	textRenderer::destroyTextRenderComponent(textRenderer, id);

	font::destroyFont(font);
}
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}
bool initializeGlew()
{
	const char * CLASS_NAME = "TestDODTextRenderer";

	HMODULE hModule = GetModuleHandle(nullptr);

	// Register the window class
	WNDCLASS wc = { };
	wc.lpfnWndProc		= WindowProc;
	wc.hInstance		= hModule;
	wc.lpszClassName	= CLASS_NAME;
	RegisterClass(&wc);

	// Create the OpenGl window
	HWND hwnd = CreateWindowEx(
		0,                              // Optional window styles.
		CLASS_NAME,                     // Window class
		"",							// Window text

		// Window style
		WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,            

		// Position
		0, 0, 
		// Size
		1, 1,

		nullptr,							// Parent window    
		nullptr,							// Menu
		hModule,							// Instance handle
		nullptr								// Additional application data
	);
	if(!hwnd)
		return false;

	g_hwnd = hwnd;

	HDC hdc = GetDC(hwnd);

	PIXELFORMATDESCRIPTOR pfd = {};
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;

	// Match appropriate pixel format in device context
	int pf = ChoosePixelFormat(hdc, &pfd);
	if(pf == 0)
	{
		return false;
	}
	if(!SetPixelFormat(hdc, pf, &pfd))
	{
		return false;
	}
	DescribePixelFormat(hdc, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

	g_hrc = wglCreateContext(hdc);
	wglMakeCurrent(hdc, g_hrc);

	if(glewInit() != GLEW_OK)
	{
		return false;
	}

	ReleaseDC(hwnd, hdc);
}
void finalizeGlew()
{
		wglMakeCurrent(nullptr, nullptr);
		wglDeleteContext(g_hrc);
		DestroyWindow(g_hwnd);
}
int main()
{
	#ifdef _DEBUG
		_CrtSetDbgFlag( _CRTDBG_REPORT_FLAG | _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_CHECK_ALWAYS_DF);
	#endif

	testCreateAndDestroyFont();
	testConvertTextToMetrics();
	testConvertTextToPrimitives();

	auto initGlewRC = initializeGlew();
	if(!initGlewRC)
		return 1;

	testConstructTextRenderComponent();

	finalizeGlew();

	#ifdef _DEBUG
	_CrtDumpMemoryLeaks();
	#endif
	return 0;
}