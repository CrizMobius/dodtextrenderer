-- Description: A simple premake script that generates a visual studio 2010 solution, containing a library and a test project.
-- Author: Christian Magnerfelt

os.mkdir("assets")
os.mkdir("bin")
os.mkdir("build")
os.mkdir("include")
os.mkdir("lib")
os.mkdir("src")
os.mkdir("temp")
os.mkdir("test")

solution "DODTextRenderer"
	configurations { "Debug", "Release" }
	platforms { "x32" }
	language "C++"
	libdirs { "$(SolutionDir)/lib" }
	flags("NoExceptions", "NoPCH")
	defines{"WIN32", "_WINDOWS", "WIN32_LEAN_AND_MEAN", "_CRT_SECURE_NO_WARNINGS"}
	
	configuration "Debug"
		defines { "_DEBUG" }
		flags { "Symbols", "StaticRuntime"}

	configuration "Release"
		defines { "NDEBUG" }
		flags { "Symbols", "Optimize", "StaticRuntime" }  
		
	-- A project defines one build target
	project "WinApp"
		location "projects"
		includedirs {  "$(SolutionDir)/src/DODTextRenderer/", "$(SolutionDir)/src/LibPNG1.6.2", "$(SolutionDir)/include/GL", "$(SolutionDir)/include/glm"}
		targetdir "$(SolutionDir)build/$(Configuration)/$(ProjectName)/"
		objdir "$(SolutionDir)temp"	
		files { "src/WinApp/**.cpp", "src/WinApp/**.hpp"}
		links { "opengl32", "DODTextRenderer", "LibPNG1.6.2", "LibZLib1.2.8", "glew32s", "glu32" } 
		kind "WindowedApp"
		flags {"WinMain"}
		postbuildcommands { "call \"$(SolutionDir)post-build.bat\" "}
			
	project "TestDODTextRenderer"
		location "projects"
		includedirs { "$(SolutionDir)/src/DODTextRenderer/", "$(SolutionDir)/src/LibPNG1.6.2", "$(SolutionDir)/include/GL"}
		targetdir "$(SolutionDir)build/$(Configuration)/$(ProjectName)/"
		objdir "$(SolutionDir)temp"		
		files { "test/test_dod_text_renderer.cpp"}
		links {"DODTextRenderer", "LibPNG1.6.2", "LibZLib1.2.8", "glew32s", "opengl32", "glu32"}
		kind "ConsoleApp"
		
	project "DODTextRenderer"
		location "projects"
		includedirs {  "$(SolutionDir)/src/DODTextRenderer/", "$(SolutionDir)/src/LibPNG1.6.2", "$(SolutionDir)/include/GL"}
		targetdir "$(SolutionDir)build/$(Configuration)/$(ProjectName)/"
		objdir "$(SolutionDir)temp"		
		files { "src/DODTextRenderer/**.cpp", "src/DODTextRenderer/**.hpp" }
		links {"LibPNG1.6.2", "LibZLib1.2.8"} 
		kind "StaticLib"

	project "LibPNG1.6.2"
		location "projects"
		includedirs { "$(SolutionDir)/src/LibZLib1.2.8/" }
		targetdir "$(SolutionDir)build/$(Configuration)/$(ProjectName)/"
		objdir "$(SolutionDir)temp"
		files { "src/LibPNG1.6.2/**.h", "src/LibPNG1.6.2/**.c" }
		links{"LibZLib1.2.8"}
		kind "StaticLib"
		
	project "LibZLib1.2.8"
		location "projects"
		targetdir "$(SolutionDir)build/$(Configuration)/$(ProjectName)/"
		objdir "$(SolutionDir)temp"
		files { "src/LibZLib1.2.8/**.h", "src/LibZLib1.2.8/**.c" }		 
		kind "StaticLib"