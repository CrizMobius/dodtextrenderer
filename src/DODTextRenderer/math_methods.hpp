/// Contains methods for operating on math types
///
///
#pragma once

#include "math_types.hpp"

namespace DODTextRenderer
{
	namespace vec4
	{
		Vec4 make(const float x, const float y, const float z, const float w);
	}
	namespace vec3
	{
		Vec3 make(const float x, const float y, const float z);
		Vec3 make(const Vec2 & vec2, const float z = 0);
	}
	namespace vec2
	{
		Vec2 make(const float x, const float y);
	}
	namespace rect
	{
		Rect make(const Vec2 & position, const Vec2 & dimension); 
	}
}