#pragma once

#include <vector>
#include <cstdint>

namespace DODTextRenderer
{
	typedef uint32_t ID;

	const uint32_t INDEX_MASK = 0xFFFF;
	const uint32_t NEW_OBJECT_ID_ADD = 0x10000;

	struct Index {
		ID id;
		unsigned short index;
		unsigned short next;
	};

	template<typename T, uint32_t MAX_OBJECTS> struct IDLookupTable
	{
		IDLookupTable() 
		{
			_num_objects = 0;
			for (uint32_t i = 0; i < MAX_OBJECTS; ++i)
			{
				_indices[i].id = i;
				_indices[i].next = i + 1;
			}
			_freelist_dequeue = 0;
			_freelist_enqueue = MAX_OBJECTS - 1;
		}

		inline bool has(ID id)
		{
			Index &in = _indices[id & INDEX_MASK];
			return in.id == id && in.index != UINT16_MAX;
		}
	
		inline T & lookup(ID id)
		{
			return _objects[_indices[id & INDEX_MASK].index];
		}
	
		inline ID add()
		{
			Index & in = _indices[_freelist_dequeue];
			_freelist_dequeue = in.next;
			in.id += NEW_OBJECT_ID_ADD;
			in.index = _num_objects++;
			T & o = _objects[in.index];
			o.id = in.id;
			return o.id;
		}
	
		inline void remove(ID id)
		{
			Index & in = _indices[id & INDEX_MASK];
		
			T & o = _objects[in.index];
			o = _objects[--_num_objects];
			_indices[o.id & INDEX_MASK].index = in.index;
		
			in.index = UINT16_MAX;
			_indices[_freelist_enqueue].next = id & INDEX_MASK;
			_freelist_enqueue = id & INDEX_MASK;
		}

		uint32_t _num_objects;
		T _objects[MAX_OBJECTS];
		Index _indices[MAX_OBJECTS];
		uint16_t _freelist_enqueue;
		uint16_t _freelist_dequeue;
	};
}