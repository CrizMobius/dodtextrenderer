// contaisn various types for rendering text
#pragma once

#include "math_types.hpp"

#include <vector>
#include <cstdint>

namespace DODTextRenderer
{
	typedef uint32_t ID;

	struct TextRenderComponent
	{
		ID				id;
		Transform4		transform;
		uint32_t		textureID;
		uint32_t		verticiesBufferID;
		uint32_t		texCoordsBufferID;
		uint32_t		indenciesBufferID;
		void *			text;
		uint32_t		textLength;
		enum Encoding {
			UTF8,
			UTF16,
		} encoding;
	};
}