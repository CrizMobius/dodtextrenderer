#include "font.hpp"

#include "memory.hpp"

#include <cstring>
#include <cstdio>
#include <png.h>

namespace DODTextRenderer
{
	namespace font
	{
		/// Header file for saving metrics to disk
		struct MetricBinaryHeader
		{
			uint32_t version;
			uint32_t length;
			uint32_t metricsStructSize;
		};
		/// The max buffer size used by the parser
		const uint32_t parserBufferMaxSize = 512;

		/// Retrieves the next key/value from a line
		bool nextMetricKeyValue(FILE * file, char * & begin, char * const key, char * const value)
		{
				char * mid = nullptr;
				char * end = nullptr;
				char * tmp = nullptr;

				mid = strchr(begin, '=');
				if(mid == nullptr) return false;

				*mid = '\0';
				mid = mid + 1;

				tmp = strrchr(begin, ' ');
				if(tmp == nullptr)
				{
					if(strlen(begin) == 0) return false;	
				}
				begin = tmp;

				end = strpbrk(mid + 1," \n");
				if(end == nullptr) return false;
				*end = '\0';
				end = end + 1;

				_snprintf(key, 32, "%s", begin);
				_snprintf(value, 32, "%s", mid);
				begin = end;
				return true;
		}
		/// Parses the next row in a metrics file
		bool parseMetricRow(FILE * file, char * buffer)
		{
				uint32_t idx = 0;
				char c = std::fgetc(file);
				buffer[idx++] = c;
				while(c != EOF && c != '\n' && idx < (parserBufferMaxSize - 1))
				{
					c = std::fgetc(file);
					buffer[idx++] = c;
				}
				if(c == EOF || idx == parserBufferMaxSize - 1) return false;	// row too long
				buffer[idx] = '\0';
				return true;
		}
		/// Parses a metrics file and stores data in a metrics datastructure
		bool parseMetricsFile(Font & f, const char * metricsFileName)
		{	
			char fontName [64];
			char buffer [parserBufferMaxSize];
			char key [32];
			char value [32];

			FILE * file = std::fopen(metricsFileName, "r");

			char * begin = buffer;
			char * mid = nullptr;
			char * end = nullptr;
			if(file != nullptr)
			{
				uint32_t count = 0;
				// Parse font name
				if(!parseMetricRow(file, buffer))
					return false;

				mid = strchr(buffer, '=');
				if(mid == nullptr) return false;

				*mid = '\0';
				mid = mid + 2;

				begin = strrchr(begin, ' ');
				if(begin == nullptr) return false;

				end = strchr(mid + 1,'\"');
				if(end == nullptr) return false;
				*end = '\0';
				end = end + 1;

				_snprintf_s(key, 32, "%s", begin);
				_snprintf_s(value, 32, "%s", mid);

				strcpy(fontName, value);

				// Parse metric count
				if(!parseMetricRow(file, buffer))
					return false;

				mid = strchr(buffer, '=');
				if(mid == nullptr) return false;

				*mid = '\0';
				mid = mid + 1;

				begin = strrchr(begin, ' ');
				if(begin == nullptr) return false;

				end = strpbrk(mid + 1," \n");
				if(end == nullptr) return false;
				*end = '\0';
				end = end + 1;

				_snprintf_s(key, 32, "%s", begin);
				_snprintf_s(value, 32, "%s", mid);
				count = atoi(value);

				// Parse metrics
				Metrics metrics = {};
				Metrics * data = new Metrics[count];
				char * p = nullptr;
				for(uint32_t i = 0; i < count; ++i)
				{
					if(!parseMetricRow(file, buffer)) return false;
					p = buffer;
					nextMetricKeyValue(file, p, key, value);
					metrics._id = std::atoi(value);
					nextMetricKeyValue(file, p, key, value);
					metrics._x1 = static_cast<float>(std::atof(value));
					nextMetricKeyValue(file, p, key, value);
					metrics._y1 = static_cast<float>(std::atof(value));
					nextMetricKeyValue(file, p, key, value);
					metrics._w = static_cast<float>(std::atof(value));
					metrics._x2 = metrics._x1 + metrics._w;
					nextMetricKeyValue(file, p, key, value);
					metrics._h = static_cast<float>(std::atof(value));
					metrics._y2 = metrics._y1 + metrics._h;
					nextMetricKeyValue(file, p, key, value);
					metrics._dx = static_cast<float>(std::atof(value));
					nextMetricKeyValue(file, p, key, value);
					metrics._dy = static_cast<float>(std::atof(value));
					nextMetricKeyValue(file, p, key, value);
					metrics._d = static_cast<float>(std::atof(value));
					data[i] = metrics;
				}
				f._metricData = data;
				f._numMetrics = count;
				return true;
			}
			return false;
		}
		/// Parses a png file and store its data and attributes in a distance field texture data structure
		bool parsePngFile(Font & f, const char * pngFileName)
		{
			png_image image = {};
			uint8_t * buffer = nullptr;
			image.version = PNG_IMAGE_VERSION;

			if(png_image_begin_read_from_file(&image, pngFileName))
			{
				image.format = PNG_FORMAT_RGBA;
				buffer = new uint8_t[PNG_IMAGE_SIZE(image)];
				if (png_image_finish_read(&image, nullptr, buffer, 0, nullptr))
				{
					DistanceFieldTextureData & d = f._dftData;
					d._width = image.width;
					d._height = image.height;
					d._buffer = buffer;
					return true;
				}
				delete [] buffer;
				return false;
			}
			return false;
		}
		bool createFont(Font & f, const char * pngFileName, const char * metricsFileName)
		{
			if(!parseMetricsFile(f, metricsFileName))
				return false;
				
			if(!parsePngFile(f, pngFileName))
				return false;

			return true;
		}
		void destroyFont(Font & f)
		{
			if(f._metricData)
			{
				delete [] f._metricData;
			}
			if(f._dftData._buffer) 
			{
				delete [] f._dftData._buffer;
			}
			std::memset(&f, 0, sizeof(Font));
		}
		void saveFontAsBinary(const Font & f, const char * fileName)
		{
			MetricBinaryHeader header = {};
			header.version = 1;
			header.metricsStructSize = sizeof(Metrics);
			header.length = f._numMetrics;

			FILE * file = std::fopen(fileName, "wb");
			std::fwrite(&header, sizeof(MetricBinaryHeader), 1, file);
			std::fwrite(f._metricData, sizeof(Metrics), f._numMetrics, file);
			std::fclose(file);
		}
		void loadFontBinary(Font & f, const char * fileName)
		{
			FILE * file = std::fopen(fileName, "wb");
			if(file != nullptr)
			{
				MetricBinaryHeader header = {};
				std::fread(&header, sizeof(MetricBinaryHeader), 1, file);
				
				if(header.metricsStructSize == sizeof(Metrics))
				{
					std::fread(f._metricData, sizeof(Metrics), header.length, file);
					std::fclose(file);
				}
				std::fclose(file);
			}
		}
	}
}