#pragma once

#include "collection_types.hpp"
#include "font_types.hpp"
#include "text_types.hpp"
#include "font.hpp"

#include <vector>

namespace DODTextRenderer
{
	struct TextRenderer
	{
		IDLookupTable<TextRenderComponent, 124> _renderComponentsLT; 
	};
	namespace textRenderer
	{
		ID constructTextRenderComponent(
			TextRenderer & textRenderer, 
			Font & font, 
			Transform4 transform,
			wchar_t * text,
			uint32_t textLength);

		void destroyTextRenderComponent(
			TextRenderer & textRenderer, 
			ID id);

		/// @brief					Converts a string of text to a buffer metrics 
		///							with a 1:1 correspondence to each character in the string
		/// @param	text			The text to convert
		/// @param	textLength		The length of the text
		/// @param	font			The font used to generate the metrics
		/// @param	metricsBuffer	Metrics buffer output
		/// @return					Returns true if successful
		///							Return false if parameters are invalid
		bool convertTextToMetrics(
			const wchar_t * text, 
			const uint32_t textLength, 
			const Font & font, 
			std::vector<Metrics> & metricsBuffer);

		///	@brief					Converts a string of text to text primtives ( verticies, texture coordinates and indencies)
		/// @param	text			The text to convert
		/// @param	textLength		The length of the text
		/// @param	font			Provides font specific data
		/// @param	fontSize		Controls text proportions
		/// @param	metricsBuffer	Metrics buffer
		/// @param	verticies		Verticies output
		/// @param	texCoords		Texture coordinates output
		/// @param	indencies		Vertex indencies output
		/// @return					Returns true if successful
		///							Return false if parameters are invalid
		bool convertTextToPrimitives(
			const wchar_t * text, 
			const uint32_t textLength, 
			const Font & font, 
			const float fontSize, 
			const std::vector<Metrics> & metricsBuffer,
			std::vector<Vec3> & verticies,
			std::vector<Vec2> & texCoords,
			std::vector<uint32_t>  & indencies);

		/// @brief	Retrives the vertex shader source code
		inline const char * getVertexShader()
		{
			const char * vs = 
				"#version 110\n"
				"\n"
				"void main()\n"
				"{\n"
				"	gl_FrontColor = gl_Color;\n"
				"	gl_TexCoord[0] = gl_MultiTexCoord0;\n"
				"\n"
				"	gl_Position = ftransform();\n"
				"}\n";
			return vs;
		}
		/// @brief	Retrieves the fragment shader source code
		inline const char * getFragmentShader()
		{
			const char * fs = 
				"#version 110\n"
				"\n"
				"uniform sampler2D	font_map;\n"
				"uniform float      smoothness;\n"
				"\n"
				"const float gamma = 2.2;\n"
				"\n"
				"void main()\n"
				"{\n"
				"	// retrieve signed distance\n"
				"	float sdf = texture2D( font_map, gl_TexCoord[0].xy ).r;\n"
				"\n"
				"	// perform adaptive anti-aliasing of the edges\n"
				"	float w = clamp( smoothness * (abs(dFdx(gl_TexCoord[0].x)) + abs(dFdy(gl_TexCoord[0].y))), 0.0, 0.5);\n"
				"	float a = smoothstep(0.5-w, 0.5+w, sdf);\n"
				"\n"
				"	// gamma correction for linear attenuation\n"
				"	a = pow(a, 1.0/gamma);\n"
				"\n"
				"	// final color\n"
				"	gl_FragColor.rgb = gl_Color.rgb;\n"
				"	gl_FragColor.a = gl_Color.a * a;\n"
				"}\n";
			return fs;
		}

	}
}