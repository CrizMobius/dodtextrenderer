#include "math_methods.hpp"

namespace DODTextRenderer
{
	namespace vec4
	{
		Vec4 make(const float x, const float y, const float z, const float w)
		{
			Vec4 vec = {x , y, z, w};
			return vec;
		}
	}
	namespace vec3
	{
		Vec3 make(const float x, const float y, const float z)
		{
			Vec3 vec = {x , y, z};
			return vec;
		}
		Vec3 make(const Vec2 & vec2, const float z)
		{
			Vec3 vec = {vec2._x, vec2._y, z};
			return vec;
		}
	}
	namespace vec2
	{
		Vec2 make(const float x = 0.0f, const float y = 0.0f)
		{
			Vec2 vec = {x , y};
			return vec;
		}
	}
	namespace rect
	{
		Rect make(const Vec2 & position, const Vec2 & dimension)
		{
			Rect rect = {position, dimension};
			return rect;
		}
	}
}