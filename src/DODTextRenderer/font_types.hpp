///	Contains all font specific types
///
///
#pragma once

#include <vector>
#include <cstdint>

namespace DODTextRenderer
{
	/// Contains the metrics of a single character
	struct Metrics
	{
		uint32_t _id;	/// The charcode
		float _x1;	/// top left of character in texture
		float _y1;	/// top left of character in texture
		float _x2;	/// bottom right of character in texture
		float _y2;	/// bottom right of character in texture
		float _w;	/// width of character
		float _h;	/// height of character
		float _dx;	/// adjusts character x positioning
		float _dy;	/// adjusts character y positioning
		float _d;	/// adjusts character x advance
	};

	/// Contains data and attributes for a distance field texture
	struct DistanceFieldTextureData
	{
		uint32_t _width;
		uint32_t _height;
		uint8_t * _buffer;
	};

	/// Contains metrics about all valid characters in a font togehter with its distance field
	struct Font
	{
		DistanceFieldTextureData _dftData;
		uint32_t _numMetrics;
		Metrics * _metricData;
	};
}