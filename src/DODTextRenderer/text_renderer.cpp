#include "memory.hpp"
#include "font_types.hpp"
#include "font.hpp"
#include "text_renderer.hpp"
#include "math_types.hpp"
#include "math_methods.hpp"

#include <algorithm>

#define GLEW_STATIC
#include <glew.h>

namespace DODTextRenderer
{
	namespace textRenderer
	{

		ID constructTextRenderComponent(
			TextRenderer & textRenderer, 
			Font & font, 
			Transform4 transform,
			wchar_t * text,
			uint32_t textLength)
		{
			TextRenderComponent component = {};
			component.transform = transform;
			component.encoding = TextRenderComponent::Encoding::UTF16;
			component.text = static_cast<void*>(text);
			component.textLength = textLength;

			std::vector<Metrics> metricsBuffer;
			metricsBuffer.reserve(textLength);
			if(!convertTextToMetrics(text, textLength, font, metricsBuffer))
			{
				return 0;
			}

			std::vector<Vec3> verticies;
			std::vector<Vec2> texCoords;
			std::vector<uint32_t> indencies;
			if(!convertTextToPrimitives(text, textLength, font, 10.0f, metricsBuffer, verticies, texCoords, indencies))
			{
				return 0;
			}

			uint32_t vertexBuffer = 0;
			uint32_t texCoordsBuffer = 0;
			glGenBuffers(1, &vertexBuffer);
			glGenBuffers(1, &texCoordsBuffer);

			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3) * verticies.size(), verticies.data(), GL_STATIC_DRAW); 
			component.verticiesBufferID = vertexBuffer;

			glBindBuffer(GL_ARRAY_BUFFER, texCoordsBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vec2) * texCoords.size(), texCoords.data(), GL_STATIC_DRAW); 
			component.texCoordsBufferID = texCoordsBuffer;
			

			GLuint textureID = 0;
			glGenTextures(1, &textureID);
			glBindTexture(GL_TEXTURE_2D, textureID);
			GLuint width = font._dftData._width;
			GLuint height = font._dftData._height;
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, font._dftData._buffer);

			component.textureID = textureID;

			ID id = textRenderer._renderComponentsLT.add();
			textRenderer._renderComponentsLT.lookup(id) = component;
			return id;
		}

		void destroyTextRenderComponent(TextRenderer & textRenderer, ID id)
		{
			if(textRenderer._renderComponentsLT.has(id))
			{
				TextRenderComponent & c = textRenderer._renderComponentsLT.lookup(id);
				if(c.textureID) glDeleteTextures(1, &c.textureID);
				if(c.verticiesBufferID) glDeleteTextures(1, &c.verticiesBufferID);
				if(c.texCoordsBufferID) glDeleteTextures(1, &c.texCoordsBufferID);

				textRenderer._renderComponentsLT.remove(id);
			}
		}
		bool convertTextToMetrics(
			const wchar_t * text, 
			const uint32_t textLength, 
			const Font & font, 
			std::vector<Metrics> & metricsBuffer)
		{
			if(text == nullptr || textLength == 0)
				return false;

			uint32_t charId = 0;
			Metrics * fontMetrics = font._metricData;
			uint32_t numMetrics = font._numMetrics;

			// Get metrics from font using character id
			for(uint32_t i = 0; i < textLength; ++i)
			{
				charId = static_cast<uint32_t>(text[i]);
				for(uint32_t j = 0; j < numMetrics; ++j)
				{
					if(charId == fontMetrics[j]._id)
					{
						metricsBuffer.push_back(fontMetrics[j]);
						break;
					}
				}
			}
			return true;
		}
		bool convertTextToPrimitives(
			const wchar_t * text, 
			const uint32_t textLength, 
			const Font & font, 
			const float fontSize, 
			const std::vector<Metrics> & metricsBuffer,
			std::vector<Vec3> & verticies,
			std::vector<Vec2> & texCoords,
			std::vector<uint32_t>  & indencies)
		{
			// Check that parameters are valid
			if(text == nullptr || textLength == 0 || fontSize <= 1.0f || metricsBuffer.size() != textLength)
				return false;

			// Get verticies, texcoords and indencies from metrics
			Metrics metrics = metricsBuffer[0];
			float advance = metrics._d * fontSize / 10.0f;

			float textureWidth = static_cast<float>(font._dftData._width);
			float textureHeight = static_cast<float>(font._dftData._height);

			Vec2 pos = vec2::make(0.0f, 0.0f);
			for(uint32_t i = 0; i < textLength; ++i)
			{
				metrics = metricsBuffer[i];

				// Skip whitespace
				switch(metrics._id)
				{
					case 32:
						pos._x += advance;
						continue;
					case 10:
						pos._y += metrics._h;
						continue;
				}

				// Construct verticies
				verticies.push_back(vec3::make(
					pos._x + metrics._dx, 
					pos._y + metrics._dy, 
					0.0f));		// Upper left corner
				verticies.push_back(vec3::make(
					pos._x + metrics._dx + metrics._w, 
					pos._y + metrics._dy, 
					0.0f));		// Upper right corner
				verticies.push_back(vec3::make(
					pos._x + metrics._dx + metrics._w, 
					pos._y + (metrics._h - metrics._dy), 
					0.0f));		// Lower right corner
				verticies.push_back(vec3::make(
					pos._x + metrics._dx, 
					pos._y + (metrics._h - metrics._dy), 
					0.0f));	// Lower left corner
				pos._x += advance;

				// Construct texcoords
				texCoords.push_back(vec2::make(metrics._x1 / textureWidth, metrics._y1 / textureHeight));	// Upper left corner
				texCoords.push_back(vec2::make(metrics._x2 / textureWidth, metrics._y1 / textureHeight));	// Upper right corners
				texCoords.push_back(vec2::make(metrics._x2 / textureWidth, metrics._y2 / textureHeight));	// Lower right corner
				texCoords.push_back(vec2::make(metrics._x1 / textureWidth, metrics._y2 / textureHeight));	// Lower left corner
			}

			return true;
		}
		
	}
}