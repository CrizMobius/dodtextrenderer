/// Contains all basic math types
///
#pragma once

namespace DODTextRenderer
{
	struct Vec4
	{
		float x;
		float y;
		float z;
		float w;
	};

	struct Vec3
	{
		float _x;
		float _y;
		float _z;
	};

	struct Vec2
	{
		float _x;
		float _y;
	};

	struct Rect
	{
		Vec2 _position;
		Vec2 _dimension;
	};

	struct Transform4
	{
		Vec4 position;
		Vec4 scale;
		Vec4 rotation;
	};
}