//////////////////////////////////////////////////////////////////////////
///	@brief	This is a data oriented textrender. The main technique used in this a renderer is called
///			signed distance fields. 
//////////////////////////////////////////////////////////////////////////

#include <windows.h>

#define GLEW_STATIC
#include <glew.h>
#include <gl/GL.h>

#include "math_types.hpp"
#include "math_methods.hpp"
#include "text_renderer.hpp"
#include "font.hpp"

#include <cstdint>
#include <vector>

const char * CLASS_NAME = "DOD Text Renderer";

using namespace DODTextRenderer;

const wchar_t * g_text = L"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
Font g_font;

uint32_t g_screenWidth = 800;
uint32_t g_screenHeight = 600;

GLuint g_vertexShader = 0;
GLuint g_fragmentShader = 0;

GLuint g_texture = 0;
GLuint g_colorBuffer = 0;
GLuint g_vertexBuffer = 0;
GLuint g_texCoordsBuffer = 0;
GLuint g_indenciesBuffer = 0;

GLuint g_program;



LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
HWND createWindow(HINSTANCE hInstance, const char * title, int x, int y, int width, int height);
bool initializeGlew(HWND hwnd, HGLRC & hrc);
bool initializeOpenGL();
void finalizeOpenGL();
void initScene();
void display(HDC hDC);

/// @brief	Application main entry point
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, INT nCmdShow)
{
	// Create the OpenGl window
	HWND hwnd = createWindow(hInstance, CLASS_NAME, 0, 0, g_screenWidth, g_screenHeight);

	if (hwnd == nullptr)
	{
		return 1;
	}

	HGLRC hrc = nullptr;
	if(!initializeGlew(hwnd, hrc))
	{
		DestroyWindow(hwnd);
		return 1;
	}

	if(!initializeOpenGL())
	{
		return 1;
	}
	initScene();

	ShowWindow(hwnd, nCmdShow);

	// Run the message loop
	MSG msg = { };
	while (true)
	{
		if(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if(msg.message == WM_QUIT)
			break;

		HDC hDC = GetDC(hwnd);
		display(hDC);
		ReleaseDC(hwnd, hDC);
	}

	finalizeOpenGL();
	DODTextRenderer::font::destroyFont(g_font);

	wglMakeCurrent(nullptr, nullptr);
	wglDeleteContext(hrc);
	DestroyWindow(hwnd);

	return msg.wParam;
}
///	@brief	Handles all window messages
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static PAINTSTRUCT ps;

	switch (uMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_PAINT:
		{
			HDC hdc = BeginPaint(hwnd, &ps);
			EndPaint(hwnd, &ps);
		}
		return 0;

	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}
///	@brief	Creates an OpenGl window 
HWND createWindow(HINSTANCE hInstance, const char * title, int x, int y, int width, int height)
{
	// Register the window class
	WNDCLASS wc = { };

	wc.style			= CS_OWNDC; 
	wc.lpfnWndProc		= WindowProc;
	wc.hInstance		= hInstance;
	wc.lpszClassName	= CLASS_NAME;

	RegisterClass(&wc);

	// Create the OpenGl window
	HWND hwnd = CreateWindowEx(
		0,                              // Optional window styles.
		CLASS_NAME,                     // Window class
		title,							// Window text

		// Window style
		WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,            

		// Position
		x, y, 
		// Size
		width, height,

		nullptr,							// Parent window    
		nullptr,							// Menu
		hInstance,						// Instance handle
		nullptr							// Additional application data
	);
	if(hwnd == nullptr)
	{
		return nullptr;
	}



	return hwnd;
}
bool initializeGlew(HWND hwnd, HGLRC & hrc)
{
	HDC hdc = GetDC(hwnd);

	PIXELFORMATDESCRIPTOR pfd = {};
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;

	// Match appropriate pixel format in device context
	int pf = ChoosePixelFormat(hdc, &pfd);
	if(pf == 0)
	{
		return false;
	}
	if(!SetPixelFormat(hdc, pf, &pfd))
	{
		return false;
	}
	DescribePixelFormat(hdc, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

	hrc = wglCreateContext(hdc);
	wglMakeCurrent(hdc, hrc);

	if(glewInit() != GLEW_OK)
	{
		return false;
	}

	ReleaseDC(hwnd, hdc);
	return true;
}
/// @brief	Initializes OpenGL shaders and buffers
bool initializeOpenGL()
{
	// Load the font
	Font font = {};
	if(!font::createFont(font, "Lucida Grande Regular.sdff.png","Lucida Grande Regular.sdff.txt"))
		return false;
	g_font = font;

	std::vector<Metrics> metricsBuffer;
	if(!textRenderer::convertTextToMetrics(g_text, std::wcslen(g_text), font, metricsBuffer))
		return false;

	std::vector<Vec3> verticies;
	std::vector<Vec2> texCoords;
	std::vector<uint32_t> indencies;

	textRenderer::convertTextToPrimitives(g_text, std::wcslen(g_text), font, 10.0f, metricsBuffer, verticies, texCoords, indencies);

	Vec4 color = vec4::make(0.0f, 1.0f, 0.0f, 1.0f);
	std::vector<Vec4> colors(verticies.size(), color);

	// Load texture
	glGenTextures(1, &g_texture);
	glBindTexture(GL_TEXTURE_2D, g_texture);
	GLuint width = font._dftData._width;
	GLuint height = font._dftData._height;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, font._dftData._buffer);

	// Bind and fill buffers
	glGenBuffers(1, &g_colorBuffer);
	glGenBuffers(1, &g_vertexBuffer);
	glGenBuffers(1, &g_texCoordsBuffer);
	//glGenBuffers(1, &g_indenciesBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, g_colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec4) * colors.size(), colors.data(), GL_STATIC_DRAW); 

	glBindBuffer(GL_ARRAY_BUFFER, g_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3) * verticies.size(), verticies.data(), GL_STATIC_DRAW); 

	glBindBuffer(GL_ARRAY_BUFFER, g_texCoordsBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec2) * texCoords.size(), texCoords.data(), GL_STATIC_DRAW); 

	//glBindBuffer(GL_ARRAY_BUFFER, g_indenciesBuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(uint32_t) * indencies.size(), indencies.data(), GL_STATIC_DRAW); 
	//glVertexAttribPointer(2, 1, GL_UNSIGNED_INT, GL_FALSE, 0, 0);

	if(g_texture == 0 || 
		g_colorBuffer == 0 || 
		g_vertexBuffer == 0 ||
		g_texCoordsBuffer == 0)
	{
		finalizeOpenGL();
		return false;
	}

	// Create program
	g_program = glCreateProgram();

	// Create shaders
	{
		g_vertexShader = glCreateShader(GL_VERTEX_SHADER);
		const char * vsSource = textRenderer::getVertexShader();
		GLint length = std::strlen(vsSource);
	
		glShaderSource(g_vertexShader, 1, &vsSource, &length); 
		glCompileShader(g_vertexShader);
	
		GLint status; 
		glGetShaderiv(g_vertexShader, GL_COMPILE_STATUS, &status);
		if(status == GL_FALSE)
		{
			finalizeOpenGL();
			return false;
		}
		glAttachShader(g_program, g_vertexShader);
	}

	{
		g_fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		const char * fsSource = textRenderer::getFragmentShader();
		GLint length = std::strlen(fsSource);
	
		glShaderSource(g_fragmentShader, 1, &fsSource, &length); 
		glCompileShader(g_fragmentShader);
	
		GLint status; 
		glGetShaderiv(g_fragmentShader, GL_COMPILE_STATUS, &status);
		if(status == GL_FALSE)
		{
			finalizeOpenGL();
			return false;
		}
		glAttachShader(g_program, g_fragmentShader);
	}
	{	
		glLinkProgram(g_program);
		GLint status;
		glGetProgramiv(g_program, GL_LINK_STATUS, &status);
		if(status != GL_TRUE)
		{
			finalizeOpenGL();
			return false;
		}
	}
	
	glUseProgram(g_program);
	auto err = glGetError();

	GLint fontMapLoc = glGetUniformLocation(g_program, "font_map");
	err = glGetError();
	GLint smoothnessLoc = glGetUniformLocation(g_program, "smoothness");

	float smoothness = 1.0f;

	glUniform1i(fontMapLoc, 0);
	err = glGetError();
	glUniform1f(smoothnessLoc, smoothness);

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, g_colorBuffer);
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, g_vertexBuffer);
	glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, 0);

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, g_texCoordsBuffer);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);
	
	glActiveTexture(GL_TEXTURE0);
	err = glGetError();
	glBindTexture(GL_TEXTURE_2D, g_texture);
	err = glGetError();

	return true;
}
/// @brief	Deletes all shaders and buffers
void finalizeOpenGL()
{
	if(g_texture) glDeleteTextures(1, &g_texture);
	if(g_colorBuffer) glDeleteBuffers(1, &g_colorBuffer);
	if(g_vertexBuffer) glDeleteBuffers(1, &g_vertexBuffer);
	if(g_texCoordsBuffer) glDeleteBuffers(1, &g_texCoordsBuffer);
	if(g_indenciesBuffer) glDeleteBuffers(1, &g_indenciesBuffer);
	if(g_program) glDeleteProgram(g_program);
	if(g_vertexShader) glDeleteShader(g_vertexShader);
	if(g_fragmentShader) glDeleteShader(g_fragmentShader);
}
/// @brief Initializes the scene
void initScene()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(50.0, 1.0, 1.0, 2000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 600.0,
			  0.0, 0.0, 0.0,
			  0.0, 1.0, 0.0);
}
///	@brief	Display all OpenGL related stuff
void display(HDC hDC)
{
	/* rotate a triangle around */
	glClear(GL_COLOR_BUFFER_BIT);

	//glBegin(GL_QUADS);
	//glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
	//glVertex2i(-400, -300);
	//glTexCoord2f(0.0, 0.0);
	//glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	//glVertex2i(400, -300);
	//glTexCoord2f(1.0, 0.0);
	//glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
	//glVertex2i(400, 300);
	//glTexCoord2f(1.0, 1.0);
	//glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
	//glVertex2i(-400, 300);
	//glTexCoord2f(0.0, 1.0);
	//glEnd();
	//glFlush();

	GLuint len = std::wcslen(g_text);
	glDrawArrays(GL_QUADS, 0, len * 4);

	SwapBuffers(hDC);
}
